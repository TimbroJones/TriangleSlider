﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace TriangleSlider
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            InitializeRainbowImage();
        }

        private Vector3[] colorsV3 = {
            new(255,0,0),
            new(255,255,0),
            new(0,255,0),
            new(0,255,255),
            new(0,0,255),
            new(255,0,255),
        };

        Dictionary<int, Color> colorDict = new();
        List<Color> colorsList = new List<Color>();
        private int colorCount = 0;
        private Vector3 previousLerp;

        private void InitializeRainbowImage()
        {
            previousLerp = colorsV3[0];
            foreach (var vector3 in colorsV3)
            {
                LerpToDictionary(vector3);
                previousLerp = vector3;
            }

            colorsList = colorDict.Values.ToList();
            colorList.ItemsSource = colorsList;
        }
        
        private void LerpToDictionary(Vector3 vector3)
        {
            Vector3 vl = previousLerp;
            do
            {
                vl = V3Lerp(vl, vector3, 1);
                colorDict.Add(colorCount++, Color.FromRgb((byte)vl.X, (byte)vl.Y, (byte)vl.Z));
            } while (!vl.Equals(vector3));
        }

        private Vector3 V3Lerp(Vector3 value1, Vector3 value2, int step)
        {
            var x = Math.Clamp(value2.X - value1.X, -1, 1);
            var y = Math.Clamp(value2.Y - value1.Y, -1, 1);
            var z = Math.Clamp(value2.Z - value1.Z, -1, 1);
            
            var v = new Vector3(x * step, y * step, z * step);

            return value1 + v;
        }

        private Vector3 V3Round(Vector3 vector3)
        {
            return new Vector3((float)Math.Ceiling(vector3.X),(float)Math.Round(vector3.Y),(float)Math.Round(vector3.Z));
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
        }

        private void Polygon_Initialized(object sender, EventArgs e)
        {
            Polygon p = (Polygon)sender;

            p.Points.Clear();
            A = new Point(0, 0);
            B = new Point(p.Width, 0);
            C = new Point(p.Width / 2, p.Height);
            p.Points.Add(A);
            p.Points.Add(B);
            p.Points.Add(C);
        }

        private Point A;
        private Point B;
        private Point C;

        private void UIElement_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition((IInputElement)sender);

            UpdateCursor(p);

            e.Handled = true;

            ((IInputElement)sender).CaptureMouse();
        }


        private void UpdateCursor(Point p)
        {

            const double OFFSET = 2.5d;
            p.Y = Math.Clamp(p.Y, 0 + Cursor.Height / 2d, Triangle.ActualHeight - Cursor.Height / 2d);
            p.Y = Math.Clamp(p.Y, 0 + Cursor.Width / 2d, Triangle.ActualWidth - Cursor.Width / 2d);

            var deltaBC = Intersection(C, B, p with { X = B.X }, p with { X = 0 });
            var deltaCA = Intersection(A, C, p with { X = 0 }, p with { X = B.X });

            p.X = Math.Clamp(p.X, deltaCA.X + Cursor.Width / 2d - OFFSET, deltaBC.X + Triangle.ActualWidth - Cursor.Width - OFFSET);


            _transform.X = p.X - Cursor.Width / 2d;
            _transform.Y = p.Y - Cursor.Height / 2d;
        }

        private Point Intersection(Point a1, Point a2, Point b1, Point b2)
        {
            // determinant
            double d = (a1.X - a2.X) * (b1.Y - b2.Y) - (a1.Y - a2.Y) * (b1.X - b2.X);

            // check if lines are parallel
            //if (Approximately(d, epsilon)) return false;

            double px = (a1.X * a2.Y - a1.Y * a2.X) * (b1.X - b2.X) - (a1.X - a2.X) * (b1.X * b2.Y - b1.Y * b2.X);
            double py = (a1.X * a2.Y - a1.Y * a2.X) * (b1.Y - b2.Y) - (a1.Y - a2.Y) * (b1.X * b2.Y - b1.Y * b2.X);

            Point p = new Point(px / d, py / d);
            return p;
        }

        /// Distance of point C from the line passing through points A and B.
        public double DistanceFromPointToLine(Point a, Point b, Point c)
        {
            double s1 = -b.Y + a.Y;
            double s2 = b.X - a.X;
            return Math.Abs((c.X - a.X) * s1 + (c.Y - a.Y) * s2) / Math.Sqrt(s1 * s1 + s2 * s2);
        }

        public bool PointInTriangle(Point p)
        {
            return PointInTriangle(A, B, C, p);
        }

        /// Determines whether point P is inside the triangle ABC
        public bool PointInTriangle(Point a, Point b, Point c, Point p)
        {
            double s1 = c.Y - a.Y;
            double s2 = c.X - a.X;
            double s3 = b.Y - a.Y;
            double s4 = p.Y - a.Y;

            double w1 = (a.X * s1 + s4 * s2 - p.X * s1) / (s3 * s2 - (b.X - a.X) * s1);
            double w2 = (s4 - w1 * s3) / s1;
            return w1 >= 0 && w2 >= 0 && (w1 + w2) <= 1;
        }


        readonly TranslateTransform _transform = new();
        private void FrameworkElement_OnInitialized(object? sender, EventArgs e)
        {
            Cursor.RenderTransform = _transform;
        }

        private void UIElement_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ((IInputElement)sender).ReleaseMouseCapture();
        }

        private void UIElement_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                return;

            Point p = e.GetPosition((IInputElement)sender);

            UpdateCursor(p);

            Mouse.Synchronize();
        }

        private void WhiteCanvas_OnInitialized(object? sender, EventArgs e)
        {
            CursorWhite.RenderTransform = _transform;
        }

        private LinearGradientBrush rainbow = new()
        {
            GradientStops = new GradientStopCollection
            {
                new(Color.FromRgb(255, 0, 0), 0),
                new(Color.FromRgb(255, 255, 0), 0.2),
                new(Color.FromRgb(0, 255, 0), 0.4),
                new(Color.FromRgb(0, 255, 255), 0.6),
                new(Color.FromRgb(0, 0, 255), 0.8),
                new(Color.FromRgb(255, 0, 255), 1),
            }
        };

        private void RangeBase_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider s = (Slider)sender;
            var d = 255 * 255 * 255;
            var ld = (int)Math.Round(Lerp(colorDict.Keys.Min(), colorDict.Keys.Max(), 1 / s.Maximum * e.NewValue));

            ColorRectangle.Fill = new SolidColorBrush(colorDict[ld]);

            //var c = Color.FromArgb(ld);

        }

        double Lerp(double firstFloat, double secondFloat, double by)
        {
            return firstFloat * (1 - by) + secondFloat * by;
        }
    }

    public class TwoDSlider : RangeBase
    {

    }
}
